package ir.hetbo.at_permissions;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.widget.Toast;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.karumi.dexter.listener.single.PermissionListener;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    View aview;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        aview = findViewById(R.id.aview);

        AppCompatButton permission3 = (AppCompatButton)findViewById(R.id.permission3);
        permission3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Dexter.withActivity(MainActivity.this)
                        .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .withListener(new MultiplePermissionsListener() {
                            @Override
                            public void onPermissionsChecked(MultiplePermissionsReport report) {
                                if (report.areAllPermissionsGranted()){
                                    Toast.makeText(MainActivity.this,"تمام دسترسی ها تایید شدند",Toast.LENGTH_SHORT).show();
                                }
                                if (report.isAnyPermissionPermanentlyDenied()){
                                    Toast.makeText(MainActivity.this,"حداقل یک دسترسی کامل رد شده است، برای تایید آن به تنظیمات برنامه بروید!",Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, final PermissionToken token) {
                                Snackbar.make(aview,"برای ادامه دادن، این دسترسی ها لازم هستند!",Snackbar.LENGTH_INDEFINITE)
                                        .setAction("اجازه دادن!", new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                token.continuePermissionRequest();
                                            }
                                        }).show();
                            }
                        }).check();
            }
        });

        AppCompatButton permission2 = (AppCompatButton)findViewById(R.id.permission2);
        permission2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Dexter.withActivity(MainActivity.this)
                        .withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .withListener(new PermissionListener() {
                            @Override
                            public void onPermissionGranted(PermissionGrantedResponse response) {
                                afterPermission();
                            }

                            @Override
                            public void onPermissionDenied(PermissionDeniedResponse response) {
                                if (response != null && response.isPermanentlyDenied()){
                                    Snackbar.make(aview,"برای ذخیره سازی، این دسترسی لازم است",Snackbar.LENGTH_LONG).setAction("اجازه دادن", new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            Intent i = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                            i.setData(Uri.fromParts("package",getPackageName(),null));
                                            startActivity(i);
                                        }
                                    }).show();
                                }else{
                                    Snackbar.make(aview,"برای ذخیره سازی، این دسترسی لازم است",Snackbar.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onPermissionRationaleShouldBeShown(PermissionRequest permission, final PermissionToken token) {
                                Snackbar.make(aview,"برای ذخیره سازی، این دسترسی لازم است",Snackbar.LENGTH_INDEFINITE)
                                        .setAction("اجازه دادن!", new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                token.continuePermissionRequest();
                                            }
                                        }).show();
                            }
                        }).check();
            }
        });

        final AppCompatButton permission = (AppCompatButton)findViewById(R.id.permission);
        permission.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED){
                    if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,Manifest.permission.CAMERA)) {
                        Snackbar.make(permission,"برای گرفتن عکس، این دسترسی لازم است",Snackbar.LENGTH_LONG).setAction("اجازه دادن", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                ActivityCompat.requestPermissions(MainActivity.this,new String[]{Manifest.permission.CAMERA},100);
                            }
                        }).show();
                    } else{
                        ActivityCompat.requestPermissions(MainActivity.this,new String[]{Manifest.permission.CAMERA},100);
                    }
                }else {
                    afterPermission();
                }
            }
        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 100){
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                afterPermission();
            }else {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)){
                    Snackbar.make(aview,"برای گرفتن عکس، این دسترسی لازم است",Snackbar.LENGTH_LONG).setAction("اجازه دادن", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ActivityCompat.requestPermissions(MainActivity.this,new String[]{Manifest.permission.CAMERA},100);
                        }
                    }).show();
                }else {
                    Snackbar.make(aview,"برای گرفتن عکس، این دسترسی لازم است",Snackbar.LENGTH_LONG).setAction("اجازه دادن", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent i = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            i.setData(Uri.fromParts("package",getPackageName(),null));
                            startActivity(i);
                        }
                    }).show();
                }
            }
        }
    }

    private void afterPermission(){
        Toast.makeText(this,"دسترسی داده شد!",Toast.LENGTH_SHORT).show();
    }
}
